// Resize.js

const sharp = require('sharp');
const uuidv4 = require('uuid/v4');
const path = require('path');

class Resize {
    
  constructor(folder, orig_filename) {
    //console.log('### Resize.constructur(%s, %s)',folder,orig_filename);
    this.folder = folder;
    this.orig_filename = orig_filename;
  }

  async save(buffer, folder, orig_filename) {
    //console.log('### Resize.save(buffer, %s, %s)',folder,orig_filename);
    this.folder = folder;
    this.orig_filename = orig_filename;
    
    const filename = Resize.filename(orig_filename);
    //console.log('### Resize.save.filename: %s',filename);
    const filepath = this.filepath(filename);
    //console.log('### Resize.save.filepath: %s',filepath);

    await sharp(buffer)
//      .resize(300, 300, {
//        fit: sharp.fit.inside,
//        withoutEnlargement: true
//      })
      .toFile(filepath);
    
    return filename;
  }

  static filename(name) {
    return name ? name : `${uuidv4()}.png`;
  }

  filepath(filename) {
    return path.resolve(`${this.folder}/${filename}`)
  }
}
module.exports = Resize;
