# upload-api

simple file/image upload API based on Node/Express

just run it with
```
$ npm install
$ npm start
```

and point your browser on [`http://localhost:300/upload`](http://localhost:300/upload) to test with the provided form or use directly the API multi-part senting a POST at the followin URL: `http://localhost:300/upload/post`



